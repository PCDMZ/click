﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Click.Controllers {

    public class UtilitiesController : Controller {

        // Animations
        public ActionResult Animations() {
            return View();
        }

        // Borders
        public ActionResult Borders() {
            return View();
        }

        // Colors
        public IActionResult Colors() {
            return View();
        }

        // Other
        public IActionResult Other() {
            return View();
        }

    } // Class

} // Namespace