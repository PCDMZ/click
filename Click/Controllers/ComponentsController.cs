﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Click.Controllers {

    public class ComponentsController : Controller {

        // Buttons
        public ActionResult Buttons() {
            return View();
        }

        // Cards
        public ActionResult Cards() {
            return View();
        }

    } // Class

} // Namespace