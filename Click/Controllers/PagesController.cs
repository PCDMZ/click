﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Click.Controllers {

    public class PagesController : Controller {

        public IActionResult Blank() {
            return View();
        }

        public IActionResult Login() {
            return View();
        }

        public IActionResult Page404() {
            return View();
        }

        public IActionResult Password() {
            return View();
        }

        public IActionResult Register() {
            return View();
        }

    } // Class

} // Namespace